# SpringDocumentation

Traducción de la documentación de Spring Framework versión 5.0.8.RELEASE y algunos puntos importantes de la misma.

Se puede revisar la [documentación original](https://docs.spring.io/spring/docs/5.0.8.RELEASE/spring-framework-reference/core.html) en la página óficial de [Spring](https://spring.io/)