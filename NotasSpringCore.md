
El registro de nuevos beans en tiempo de ejecución no es oficialmente soportado.

Los beans deben ser únicos en nombre en el contenedor que en el que se almacenan
Para setear los nombres de los beans en una configuración basada en XML se utilizan los atributos name o id.

Como regla general utilizar constructores para dependencias obligatorias y métodos setter o metodos de configuración
para dependencias opcionales.

Se recomienda siempre utilizar la inyección de dependencias por constructor debido a que permite implementar los
componentes de la aplicación como objetos inmutables y se asegura que la dependencias obligatorias no sean nulas.

