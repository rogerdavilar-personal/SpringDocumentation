Los packages org.springframework.beans and org.springframework.context son la base del contenedor IoC de Spring Framework

La interface BeanFactory provee un mecanismo de configuración avanzado capaz de administrar cualquier tipo de objeto.
El ApplicationContext es una sub-interface de BeanFactory y agrega una integración más sencilla con las caracteristicas
de Spring AOP; manejo de recursos de mensaje (utilizado para internacionalización), publicación de eventos, y contexto 
especifico en la capa de aplicación como el WebApplicationContext para uso en aplicaciones web.

El BeanFactory provee el framework de configuración y funcionalidad básica, y el ApplicationContext agrega mas
funcionalidades especificas de empresas. El ApplicationContext es un superconjunto completo de la interface BeanFactory y es
utilizado exclusivamente en este capitulo en descripciones del contenedor IoC de Spring.

En Spring, los objetos que forman el esqueleto de tu aplicación y que son administrados por el contenedor IoC de Spring 
son llamados beans. Un bean es un objeto que es instanciado, ensamblado, y de otrnera administrado por el contenedor
IoC de Spring. Los beans y las dependencias entre ellos, son reflejados en la metadata de configuración utilizada por
el container.

1.2 Container overview
La interface org.springframework.context.ApplicationContext representa el contenedor IoC de Spring y es responsable de 
instanciar, configurar, y ensamblar los ya mencionados beans. El contenedor obtiene instrucciones sobre que objetos
instanciar, configurar, y ensamblar leyendo la metadata de configuración. La metadata de configuración es representada
en XML, anotaciones Java, o código Java. Esto te permite expresar los objetos que componen tu aplicación y las
interdependencias entre estos objetos.

Muchas implementaciones de la interfaz ApplicationContext son suministradas por default con Spring. En aplicaciones
standalone es comun crear una instancia de ClassPathCmlApplicaionContext o FileSystemXmlApplicationContext. 
Mientras XML ha sido el formato tradicional para definir la metadata de configuración puedes instruir al contenedor
para que utilice anotaciones Java o codigo como formato de la metadata suministrando una pequeña cantidad de 
configuración XML para habilitar el soporte para estos formatos adicionales de metadata.

En la mayoria de los escenarios de aplicación, el código explicito del usuario no es requerido para instanciar una o
mas instancias de un contenedor IoC de Spring. Por ejemplo, en un escenario de una aplicación web, 8 lineas (mas o menos)
en el archivo web.xml de la alicación tipicamente serán suficientes. Si estas utilizand el Entorno de desarrollo STS, esta
configuración descriptiva puede ser facilmente creada con pocos clicks de mouse o pulsaciones de teclas.

El siguiente diagrama es una vista a alto nivel acerca de como trabaja Spring. Las clases de tu aplicación son combinadas
con la metadata de configuración para que después el ApplicationContext sea creado e inicializado, tu tienes un sistema o
aplicación completamente configurado y ejecutable.

1.2.1 Metadata de configuración
Como muestra el diagrama anterior, el contenedor IoC de Spring consume un formulario de metadata de configuración; esta
metadata de configuración representa como tu, como un desarrollador de aplicaciones, le dices al contenedor de Spring que 
instancie, configure y ensamble los objetos en tu aplicación.

La metadata de configuración es tradicionalmente suministrada en un simple e intuitivo formato XML, que es lo que la
mayor parte de este capitulo utiliza para transmitir conceptos y caracteristicas clave del contenedor IoC de Spring.

Nota: La metadata basada en XML no es la unica forma permitida para la metadata de configuración. El contenedor IoC de
Spring en si mismo esta totalmente desacoplado del formato en que esta metadata de configuración es escrita. Estos días,
muchos desarrolladores escogen configuración basada en Java para sus aplicaciónes Spring.

La configuración de Spring consiste de al menos una y tipicamente mas de una definición de bean que el container debe
administrar. La metadata de configuración basada en XML muestra estos beans configurados como elementos <bean/> dentro
del elemento de nivel superior <beans/>. La configuración basada en Java, tipicamente utiliza los métodos anotados con
@Bean dentro de una clase anotada con @Configuration.

Estas definiciones de bean corresponden a los objetos actuales que crean tu aplicación. Por lo general, tu definies los
objetos de la capa de servicio, los objetos de acceso a datos (DAOs), objetos de presentación (como las instancias Action
de Struts), objetos de infraestructura como Hibernate SessionFactories, JMS Queues, and so forth. Por lo general uno no
configura los objetos de dominio granulares en el contenedor, por que usualmente es responsabilidad de los DAOs y
la lógica de negocio que creen y carguen los objetos de dominio. Como sea, puedes utilizar la integración de Spring con
AspectJ para configurar objetos que han sido creados fuera del control de un contenedor IoC.

1.2.2 Instanciando un contenedor
Instanciar un contenedor IoC de Spring es sencillo. El path de ubicación o paths suministrados a un constructor de
ApplicationContext son actualmente cadenas de recursos que permiten al contenedor carga la metadata de configuración de
una variedad de recursos externos como el sistema de archivos local, el CLASSPATH de Java, etcétera.

ApplicationContext context = new ClassPathXmlApplicationContext("services.xml", "daos.xml");

Nota: Después de aprender sobre el contenedor IoC de Spring, es posible que desees conocer mas acerca de 
la abstracción Resource de Spring, como se describió en Recursos, que provee un mecanismo conveniente para leer un
InputStream desde las ubicaciones definidass en una sintaxis URI. En particular, los paths Resource son utilizados para
construir contextos de aplicaciones como se describió en Contextos de aplicaciones y paths de recursos.

[....]







1.3.2 Instanciando beans
Una definición de bean escencialmente es una receta para crear uno o mas objetos. El contenedor mira la receta para un
bean nombrado cuando lo solicita, y utiliza la metadata de configuración encapsulada por la definición del bean para
crear (o adquirir) un objeto actual.

Si utilizas metadata de configuración basada en XML, especificas el tipo (o clase) de objeto que se va a instanciar en
el atributo clase del elemento <bean/>. Este atributo clase, que internamente es una propiedad Class en una instancia de
BeanDefinition, es usualmente obligatorio. Utilizas la propiedad Class en una de las dos maneras:
    - Generalmente, para especificar la clase del bean que se va a construir en el caso donde el contenedor en mismo 
    crea directamente el bean llamando a su constructor en forma reflexiva, algo equivalente al código Java 
    con el operador new.
    - Para especificar la clase actual que contiene el metodo estatico factory que será invocado para crear el objeto,
    en el caso menos común donde el container invoca un metodo estatico factory en una clase para crear el bean. El
    tipo de objeto retornado por la invocación del metodo estatico factory puede ser la misma clase u otra clase por 
    completo.

[....]

El proceso de resolución de dependencias
El container ejecuta la resolución de dependencias de bean de la siguiente manera:
    - El ApplicationContext es creado e inicializado con la metadata de configuración que describe todos los beans. La
    metadata de configuración puede ser especificada via XML, código Java, o anotaciones.
    - Por cada bean, sus dependencias son expresadas en forma de propiedades, argumentos de constructor, o argumentos
    al método estatico factory si estas utilizandolo en lugar del constructor normal. Estas dependencias son suministradas
    al bean, cuando el bean esta actualmente creado
    - Cada propiedad o argumento de constructor es una definición actual del valor a asignar, o una referencia a otro bean
    en el container.
    - Cada propiedad o argumento de constructor que es un valor es convertido de su formato especifico al tipo actual de
    esa propiedad o argumento de constructor. Por default Spring puede convertir un valor suministrado en formato string
    a todos los tipos incorporados tal como int, long, String, boolen, etc.

El container de Spring valida la configuración de cada bean a medida que el container es creado. Sin embargo, las propiedades
de los beans en si mismas no son asignadas hasta que el bean es realmente creado. Los beans que son de alcance singleton y
estan listos para ser preinstanciados (default) son creados cuando el container es creado. De lo contrario, el bean es
creado solo cuando este es solicitado. La creación de un bean puede causar la creación de un grafo de beans a ser creados,
a medida que se crean y se asignan las dependencias del bean y sus dependencias y sus dependencias (etcetera).